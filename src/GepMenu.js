import React, {Component} from 'react';
import searchIcon from './search-icon.png';
import {NavLink} from 'react-router-dom'
import './GepMenu.css';

class GepMenu extends Component {

    render(){
        let linksMarkup = this.props.links.map((link, index) => {
            let linkMarkup = <NavLink to={link.link} className="menu__link" activeClassName="menu__link--active" >{link.label}</NavLink>
            return(
                <li key={index} className="menu__list-item"> {linkMarkup} </li>
            );
        });

        return (
            <nav className="menu">
                <h1 style = {{
                    backgroundImage : 'url(' + this.props.logo +')'
                }} className="menu__logo">G</h1>

                <div className="menu__right">
                    <ul className="menu__list">
                        {linksMarkup}
                    </ul>

                    <button style = {{
                        backgroundImage: 'url(' + searchIcon + ')'
                    }} className="menu__search-button" />

                    <form className="menu__search-form hide" method="POST">
                        <input className="menu__search-input" placeholder="Type and hit enter"/>
                    </form>
                </div>
            </nav>
        );
    }

}

export default GepMenu;