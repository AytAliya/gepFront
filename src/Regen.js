import React from 'react'
import Layout from './Layout'
import './Regen.css';
class Regen extends React.Component{

    constructor() {
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        const data = new FormData(event.target);

        console.log('identifierType: ', data.get('identifierType'));
        console.log('identtifier: ', data.get('identtifier'));
        console.log('genType: ', data.get('genType'));
        console.log('genXls: ', data.get('genXls'));
        console.log('genF119: ', data.get('genF119'));
    }

    render(){
        return (
            <Layout>
                <div className="form-style-2">
                    <div className="form-style-2-heading"></div>
                    <form onSubmit={this.handleSubmit}>
                        <label htmlFor="field1"><span>Тип идентификатора</span><select name="identifierType" className="select-field">
                            <option value="Code_package">Code_package</option>
                            <option value="Barcode">Barcode</option>
                            <option value="Readydoc_id">Readydoc_id</option></select></label>

                        <label htmlFor="field2"><span>Идентификатор<span className="required">*</span></span><textarea name="identtifier" className="textarea-field"/></label>

                        <label htmlFor="field3"><span>Способ перегенрации</span><select name="genType" className="select-field">
                            <option value="General Question">Оставить тот же ШПИ</option>
                            <option value="Advertise">Перегенерить ШПИ</option></select></label>


                        <label htmlFor="field4"><span className="checkBox">Сгенерировать Forms103(xls)</span><input type="checkbox"
                                                                           className="input-field"
                                                                           name="genXls"
                                                                           value="1"/></label>

                        <label htmlFor="field5"><span className="checkBox">Сгенерировать F119</span><input type="checkbox"
                                                                           className="input-field"
                                                                           name="genF119"
                                                                           value="1"/></label>

                        <label><input type="submit" value="Submit"/></label>
                    </form>
                </div>

            </Layout>
        )
    }

}

export default Regen;