import React from 'react'
import {BrowserRouter, Switch, Route} from 'react-router-dom';

import Unprocessed from "./Unprocessed"
import Orgs from "./Orgs"
import Regen from "./Regen";

function Routes() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact={true} component={Regen}/>
                <Route path="/about" exact={true} component={Unprocessed}/>
                <Route path="/orgs" exact={true} component={Orgs} />
            </Switch>
        </BrowserRouter>
    )
}

export default Routes;