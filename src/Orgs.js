import React from 'react'
import Layout from './Layout'
import './Orgs.css';
class Orgs extends React.Component{

    constructor() {
        super()
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            isHidden: true,
            orgsList: [
                {senderId: '141', name: 'Юр лица Картел', BIN: '980540000397', CODE: 'POSTKZMA'},
                {senderId: '5', name: 'Верховный Суд РК 2', BIN: '', CODE: 'VS2'},
                {senderId: '7', name: 'Система MailKZ', BIN: '', CODE: 'MAILKZ'},
                {senderId: '701', name: 'ТОО "ФА Альянс Финанс"', BIN: '060840007998', CODE: 'POSTKZMA'},
                {senderId: '621', name: 'ТОО "MONEY-M"', BIN: '171140025532', CODE: 'POSTKZMA'},
                {senderId: '541', name: 'ГУ Департамент внутрених дел города Астаны Министерства внутрених дел Республики Казахстан', BIN: '', CODE: 'DVD'}
            ]
        }
    }

    toggleHidden(){
        console.log('CLICKED')
        this.setState({
            isHidden: !this.state.isHidden
        })
    }

    handleSubmit(event) {
        console.log('HAAAANDLE', event)
        event.preventDefault();
        const data = new FormData(event.target);

        console.log('orgName: ', data.get('orgName'));
        console.log('orgBIN: ', data.get('orgBIN'));
    }

    render(){

        const orgs = this.state.orgsList;

        return (
            <Layout>
                <div>
                    <a onClick={this.toggleHidden.bind(this)}> Добавить новую организацию</a>
                </div>
                {!this.state.isHidden && <AddOrg />}
                <div>
                    {!orgs.length ? null : (
                        <table className="uk-table uk-table-small uk-table-divider uk-table-middle">
                            <thead>
                            <tr>
                                <th className="uk-table-shrink" />
                                <th className="uk-table-expand">senderId</th>
                                <th className="uk-width-small uk-visible@m">name</th>
                                <th className="uk-width-small uk-visible@m">BIN</th>
                                <th className="uk-width-small uk-visible@m">CODE</th>
                            </tr>
                            </thead>
                            <tbody>
                            {orgs.map((form103, index) => (
                                <tr key={index}>
                                    <td><p className="uk-text-small uk-margin-remove">{index + 1}</p></td>
                                    <td><p className="uk-text-small uk-margin-remove">{orgs.senderId === null ? '-' : orgs.senderId}</p></td>
                                    <td><p className="uk-width-small uk-visible@m">{orgs.name === null ? '-' : orgs.name}</p></td>
                                    <td><p className="uk-width-small uk-visible@m">{orgs.BIN === null ? '-' : orgs.BIN}</p></td>
                                    <td><p className="uk-width-small uk-visible@m">{orgs.CODE === null ? '-' : orgs.CODE}</p></td>
                                </tr>
                            ))}
                            </tbody>
                        </table>
                    )}
                </div>
            </Layout>
        )
    }


}

const AddOrg = () => (
    <div className="form-style-3">
        <div className="form-style-3-heading"></div>
        <form onSubmit={this.handleSubmit}>
            <label htmlFor="field1"><span>Наименование Организации: </span><input type="text" className="input-field" name="orgName" /></label>

            <label htmlFor="field2"><span>БИН: </span><input type="text" className="input-field" name="orgBIN" /></label>

            <label><input type="submit" value="Submit"/></label>
        </form>
        <div className="form-style-3-heading"></div>
    </div>
)

export default Orgs;


