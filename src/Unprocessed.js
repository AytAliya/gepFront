import React from 'react'
import Layout from './Layout'
class Unprocessed extends React.Component{

    constructor(props){
        super(props);

        this.state= {
            unprocList: [
                {codePackage: '187100033660209', f5: '010000', senderId: '541', idTypeDoc: '602', sysDate: '02.08.2018'},
                {codePackage: '187100033660214', f5: '010000', senderId: '541', idTypeDoc: '602', sysDate: '12.07.2018'},
                {codePackage: '187100033660216', f5: '010000', senderId: '541', idTypeDoc: '602', sysDate: '03.07.2018'},
                {codePackage: '187100033660217', f5: '010000', senderId: '541', idTypeDoc: '602', sysDate: '01.08.2018'},
                {codePackage: '187100033660220', f5: '010000', senderId: '541', idTypeDoc: '602', sysDate: '02.08.2018'},
                {codePackage: '187100033660222', f5: '010000', senderId: '541', idTypeDoc: '602', sysDate: '02.07.2018'},
                {codePackage: '187100033660224', f5: '010000', senderId: '541', idTypeDoc: '602', sysDate: '22.06.2018'},
                {codePackage: '187100033660226', f5: '010000', senderId: '541', idTypeDoc: '602', sysDate: '12.04.2018'},
                {codePackage: '187100033660566', f5: '010000', senderId: '541', idTypeDoc: '602', sysDate: '02.08.2018'},
                {codePackage: '187100033660571', f5: '010000', senderId: '541', idTypeDoc: '602', sysDate: '02.08.2018'}
            ]
        }
    }

    render(){

        const {unprocList} = this.state;

        return (
            <Layout>
                <p>Количество не сгенерированных писем: {unprocList.length}</p>
                <div>
                    {!unprocList.length ? null : (
                        <table className="uk-table uk-table-small uk-table-divider uk-table-middle">
                            <thead>
                            <tr>
                                <th className="uk-table-shrink" />
                                <th className="uk-table-expand">Code_package</th>
                                <th className="uk-width-small uk-visible@m">F5</th>
                                <th className="uk-width-small uk-visible@m">Sender_id</th>
                                <th className="uk-width-small uk-visible@m">Id_Type_Doc</th>
                                <th className="uk-width-small uk-visible@m">Sys_date</th>
                            </tr>
                            </thead>
                            <tbody>
                            {unprocList.map((form103, index) => (
                                <tr key={index}>
                                    <td><p className="uk-text-small uk-margin-remove">{index + 1}</p></td>
                                    <td><p className="uk-text-small uk-margin-remove">{form103.codePackage === null ? '-' : form103.codePackage}</p></td>
                                    <td><p className="uk-width-small uk-visible@m">{form103.f5 === null ? '-' : form103.f5}</p></td>
                                    <td><p className="uk-width-small uk-visible@m">{form103.senderId === null ? '-' : form103.senderId}</p></td>
                                    <td><p className="uk-width-small uk-visible@m">{form103.idTypeDoc === null ? '-' : form103.idTypeDoc}</p></td>
                                    <td><p className="uk-width-small uk-visible@m">{form103.sysDate === null ? '-' : form103.sysDate}</p></td>
                                </tr>
                            ))}
                            </tbody>
                        </table>
                    )}
                </div>
            </Layout>
        )
    }
}

export default Unprocessed;