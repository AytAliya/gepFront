import React from 'react';
import {Provider} from 'react-redux'
import Routes from './router'
import './App.css';

export default ()=> (
    <Provider>
        <Routes/>
    </Provider>
        )
