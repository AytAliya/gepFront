import React, { Component } from 'react';

import Header from './Header'

class Layout extends Component {
    render() {

        return (
            <React.Fragment>
                <Header/>
                <main className="uk-flex-auto">
                    {this.props.children}
                </main>
            </React.Fragment>
        );
    }
}

export default Layout;