import React from 'react'
import logo from './favicon.png';
import GepMenu from './GepMenu';

class Header extends React.Component{
    constructor(props){
        super(props);

        this.state= {
            links: [
                {label: 'Перегенерация', link: '/'},
                {label: 'Необработанные', link: '/about'},
                {label: 'Организации', link: '/orgs'}
            ]
        }
    }

    render(){
        const {links} = this.state;
        return(
            <div className="container center">
                <GepMenu links={links} logo={logo} />
            </div>
        )
    }
}

export default Header